package nz.co.elysium.core;

import java.util.HashSet;

public class PizzaOrder {
    private final long id;
    private final Pizza pizza;
    private final HashSet<Topping> toppings;
    private final PizzaBase base;
    private Long price;

    public PizzaOrder(long id, Pizza pizza, PizzaBase base) {
        this.id = id;
        this.pizza = pizza;
        this.toppings = new HashSet<>(pizza.getToppings());
        this.base = base;
        updatePrice();
    }

    public void addTopping(Topping t) {
        toppings.add(t);
    }

    public void removeTopping(Topping t) {
        toppings.remove(t);
    }

    public void updatePrice() {
        price = pizza.getPrice() + base.getPrice();
        HashSet<Topping> inst_toppings = new HashSet<>(toppings);
        inst_toppings.removeAll(pizza.getToppings());
        price += 200 * inst_toppings.size();
    }

    public long getId() {
        return id;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public HashSet<Topping> getToppings() {
        return toppings;
    }

    public PizzaBase getBase() {
        return base;
    }

    public Long getPrice() {
        return price;
    }
}
