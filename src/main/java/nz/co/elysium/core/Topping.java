package nz.co.elysium.core;

public class Topping {
    private long id;
    private String name;

    public Topping() {
    }

    public Topping(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
