package nz.co.elysium.core;

import java.util.ArrayList;
import java.util.Date;

public class Order {
    private final long id;
    private String customerName;
    private String customerPhone;
    private ArrayList<Pizza> pizzas;
    private Long price;
    private OrderStatus status;
    private Date orderDate;

    public Order(long id) {
        this.id = id;
        this.customerName = null;
        this.customerPhone = null;
        this.price = Long.valueOf(0);
        this.pizzas = new ArrayList<>();
        this.status = OrderStatus.PENDING;
        this.orderDate = null;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public void confirmOrder() {
        status = OrderStatus.CONFIRMED;
        orderDate = new Date();
    }

    public void setPizzas(ArrayList<Pizza> pizzas) {
        this.pizzas = pizzas;
    }

    public void updatePrice() {
        // calculate price
        price = Long.valueOf(0);
    }
}
