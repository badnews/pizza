package nz.co.elysium.core;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ToppingMapper implements ResultSetMapper<Topping> {
    public Topping map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        return new Topping(r.getInt("id"), r.getString("name"));
    }
}
