package nz.co.elysium.core;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(ToppingMapper.class)
public interface ToppingDAO {

    @SqlUpdate("insert into toppings (name) values (:name)")
    @GetGeneratedKeys
    long insert(@Bind("name") String name);

    @SqlQuery("select * from toppings")
    List<Topping> selectAll();

    @SqlQuery("select * from toppings where id = :id")
    Topping selectById(@Bind("id") int id);

    @SqlUpdate("delete from toppings")
    void deleteAll();
}
