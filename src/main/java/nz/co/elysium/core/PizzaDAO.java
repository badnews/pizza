package nz.co.elysium.core;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

/*
create table pizza (id serial,
                    name varchar(255) not null,
                    price integer not null);

create table pizza_toppings (pizza_id integer not null,
                             topping_id integer not null);
 */

@RegisterMapper(PizzaMapper.class)
public interface PizzaDAO {

    @SqlUpdate("insert into pizza (name, price) values (:name, :price)")
    @GetGeneratedKeys
    long insert(@Bind("name") String name, @Bind("price") long price);

    @SqlUpdate("insert into pizza_toppings (pizza_id, topping_id) values " +
               "(:pizza_id, :topping_id)")
    long insertTopping(@Bind("pizza_id") int id,
                       @Bind("topping_id") int topping_id);

    @SqlUpdate("delete from pizza_toppings where pizza_id = :pizza_id")
    void deleteToppings(@Bind("pizza_id") int id);

    @SqlUpdate("delete from pizza_toppings where pizza_id = :pizza_id " +
               "and topping_id = :topping_id")
    void deleteTopping(@Bind("pizza_id") int id,
                       @Bind("topping_id") int topping_id);

    @SqlQuery("select id, name from toppings, pizza_toppings where " +
               "pizza_id = :pizza_id and toppings.id = topping_id")
    @RegisterMapper(ToppingMapper.class)
    List<Topping> selectToppings(@Bind("pizza_id") int id);

    @SqlQuery("select * from pizza where id = :id")
    Pizza selectById(@Bind("id") int id);

    @SqlQuery("select * from pizza")
    List<Pizza> selectAll();

    @SqlUpdate("delete from pizza")
    void deletePizza();

    @SqlUpdate("delete from pizza_toppings")
    void deletePizzaToppings();
}
