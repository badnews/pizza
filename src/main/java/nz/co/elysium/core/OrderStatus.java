package nz.co.elysium.core;

public enum OrderStatus {
    PENDING,
    CONFIRMED,
    PREPARING,
    COOKING,
    READY,
    COLLECTED;
}
