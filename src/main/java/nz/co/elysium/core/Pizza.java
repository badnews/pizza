package nz.co.elysium.core;

import java.util.ArrayList;

public class Pizza {
    private final long id;
    private final String name;
    private final ArrayList<Topping> toppings;
    private final Long price;

    public Pizza(long id, String name, Long price) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.toppings = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getPrice() {
        return price;
    }

    public ArrayList<Topping> getToppings() {
        return toppings;
    }

    public void addTopping(Topping t) {
        toppings.add(t);
    }
}
