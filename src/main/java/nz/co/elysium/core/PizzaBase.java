package nz.co.elysium.core;

public enum PizzaBase {
    THIN("Thin & Crispy", 0),
    PAN("Pan", 0),
    CHEESE_CRUST("Cheese Crust", 200);

    private final String name;
    private final long price;

    private PizzaBase(String name, long price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public long getPrice() {
        return price;
    }
}
