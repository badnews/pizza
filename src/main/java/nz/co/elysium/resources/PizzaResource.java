package nz.co.elysium.resources;

import com.yammer.metrics.annotation.Timed;
import nz.co.elysium.core.Pizza;
import nz.co.elysium.core.PizzaDAO;
import nz.co.elysium.core.Topping;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/pizzas")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PizzaResource {
    private final PizzaDAO dao;

    public PizzaResource(PizzaDAO dao) {
        this.dao = dao;
    }

    private void injectToppings(Pizza p) {
        List<Topping> toppings = dao.selectToppings((int)p.getId());
        for (Topping t : toppings) {
            p.addTopping(t);
        }
    }

    @GET
    @Timed
    @Path("{id}")
    public Pizza getPizza(@PathParam("id") int id) {
        Pizza p = dao.selectById(id);
        injectToppings(p);
        return p;
    }

    @GET
    @Timed
    public List<Pizza> getPizzas() {
        List<Pizza> pizzas = dao.selectAll();
        for (Pizza p : pizzas) injectToppings(p);
        return pizzas;
    }

    @POST
    @Timed
    @Path("{id}/toppings")
    public Pizza addTopping(@PathParam("id") int id,
                            @QueryParam("topping_id") int topping_id) {
        Pizza p = dao.selectById(id);
        dao.insertTopping(id, topping_id);
        injectToppings(p);
        return p;
    }

    @DELETE
    @Timed
    @Path("{id}/toppings/{topping_id}")
    public Pizza deleteTopping(@PathParam("id") int id,
                               @PathParam("topping_id") int topping_id) {
        Pizza p = dao.selectById(id);
        dao.deleteTopping(id, topping_id);
        injectToppings(p);
        return p;
    }
}
