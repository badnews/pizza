package nz.co.elysium.resources;

import com.yammer.metrics.annotation.Timed;
import nz.co.elysium.core.Topping;
import nz.co.elysium.core.ToppingDAO;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Path("/toppings")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ToppingResource {
    private final AtomicLong counter;
    private final ToppingDAO dao;

    public ToppingResource(ToppingDAO dao) {
        this.counter = new AtomicLong();
        this.dao = dao;
    }

    @POST
    @Timed
    public Topping createTopping(@Valid Topping topping) {
        long id = dao.insert(topping.getName());
        topping.setId(id);
        return topping;
    }

    @GET
    @Timed
    public List<Topping> getToppings() {
        return dao.selectAll();
    }

    @GET
    @Timed
    @Path("{id}")
    public Topping getTopping(@PathParam("id") int id) {
        return dao.selectById(id);
    }
}
