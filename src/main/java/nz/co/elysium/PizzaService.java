package nz.co.elysium;

import com.yammer.dropwizard.Service;
import com.yammer.dropwizard.config.Bootstrap;
import com.yammer.dropwizard.config.Environment;
import com.yammer.dropwizard.jdbi.DBIFactory;
import nz.co.elysium.cli.Seeder;
import nz.co.elysium.core.PizzaDAO;
import nz.co.elysium.core.ToppingDAO;
import nz.co.elysium.resources.PizzaResource;
import nz.co.elysium.resources.ToppingResource;
import org.skife.jdbi.v2.DBI;

public class PizzaService extends Service<PizzaConfiguration> {

    public static void main(String[] args) throws Exception {
        new PizzaService().run(args);
    }

    @Override
    public void initialize(Bootstrap<PizzaConfiguration> bootstrap) {
        bootstrap.addCommand(new Seeder());
    }

    @Override
    public void run(PizzaConfiguration config, Environment environment) throws ClassNotFoundException {
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, config.getDatabaseConfiguration(), "postgresql");
        final ToppingDAO dao = jdbi.onDemand(ToppingDAO.class);
        final PizzaDAO pizza_dao = jdbi.onDemand(PizzaDAO.class);
        environment.addResource(new ToppingResource(dao));
        environment.addResource(new PizzaResource(pizza_dao));
    }
}
