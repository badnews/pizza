package nz.co.elysium.cli;

import com.yammer.dropwizard.cli.EnvironmentCommand;
import com.yammer.dropwizard.config.Environment;
import com.yammer.dropwizard.jdbi.DBIFactory;
import net.sourceforge.argparse4j.inf.Namespace;
import nz.co.elysium.PizzaConfiguration;
import nz.co.elysium.PizzaService;
import nz.co.elysium.core.PizzaDAO;
import nz.co.elysium.core.ToppingDAO;
import org.skife.jdbi.v2.DBI;

public class Seeder extends EnvironmentCommand<PizzaConfiguration> {

    public Seeder() {
        super(new PizzaService(), "seeder", "data seeding");
    }

    @Override
    protected void run(Environment environment, Namespace namespace, PizzaConfiguration config) throws Exception {
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, config.getDatabaseConfiguration(), "postgresql");
        final ToppingDAO dao = jdbi.onDemand(ToppingDAO.class);
        final PizzaDAO pizza_dao = jdbi.onDemand(PizzaDAO.class);

        // toppings
        dao.deleteAll();
        long t_mushroom = dao.insert("Mushroom");
        long t_onion = dao.insert("Onion");
        long t_olive = dao.insert("Olives");
        long t_pineapple = dao.insert("Pineapple");
        long t_ham = dao.insert("Ham");
        long t_cheese = dao.insert("Cheese");

        // pizzas
        pizza_dao.deletePizza();
        pizza_dao.deletePizzaToppings();
        long p_hawaiian = pizza_dao.insert("Hawaiian", 1290);
        pizza_dao.insertTopping((int)p_hawaiian, (int)t_cheese);
        pizza_dao.insertTopping((int)p_hawaiian, (int)t_ham);
        pizza_dao.insertTopping((int)p_hawaiian, (int)t_pineapple);
        long p_vege = pizza_dao.insert("Vegetarian", 890);
        pizza_dao.insertTopping((int)p_vege, (int)t_cheese);
        pizza_dao.insertTopping((int)p_vege, (int)t_onion);
        pizza_dao.insertTopping((int)p_vege, (int)t_olive);
        pizza_dao.insertTopping((int)p_vege, (int)t_mushroom);
    }
}
