package nz.co.elysium;

import nz.co.elysium.core.Pizza;
import nz.co.elysium.core.PizzaBase;
import nz.co.elysium.core.PizzaOrder;
import nz.co.elysium.core.Topping;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PizzaOrderTest {

    Topping t1, t2, t3;
    Pizza pizza;

    @Before
    public void setUp() throws Exception {
        // set up
        t1 = new Topping(1, "Cheese");
        t2 = new Topping(2, "Ham");
        t3 = new Topping(3, "Pineapple");
        pizza = new Pizza(3, "PizzaOne", (long)1000);
        pizza.addTopping(t1);
        pizza.addTopping(t2);
    }

    @Test
    public void makePizzaOrderDefault() {
        // create order
        PizzaOrder po = new PizzaOrder(7, pizza, PizzaBase.THIN);
        assertEquals(Long.valueOf(po.getPrice()), Long.valueOf(1000));
    }

    @Test
    public void makePizzaOrderExtraTopping() {
        // create order
        PizzaOrder po = new PizzaOrder(7, pizza, PizzaBase.THIN);
        po.addTopping(t3);
        po.updatePrice();
        assertEquals(Long.valueOf(po.getPrice()), Long.valueOf(1200));
    }

    @Test
    public void makePizzaOrderRemoveTopping() {
        // create order
        PizzaOrder po = new PizzaOrder(7, pizza, PizzaBase.THIN);
        po.removeTopping(t2);
        po.updatePrice();
        assertEquals(Long.valueOf(po.getPrice()), Long.valueOf(1000));
    }
}
